const express = require('express');
const app = express();


// Get dependencies
const path = require('path');
const http = require('http');

// Point static path to dist
app.use(express.static(path.join(__dirname, 'build')));

// Catch all other routes and return the index file
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '/build/index.html'));
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '8080';
const ip = process.env.IP || '0.0.0.0';
app.set('port', port);
app.set('ip', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on 0.0.0.0:${port}`)); 
  